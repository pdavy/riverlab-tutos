tests=(elder_creek)
call .\config.bat

for %%f in tests do (
	cd %%f
	call ..\filename.bat  *.bat
	start /NORMAL %eros%.exe dir=%filename%_test/ test.arg pause=1
	cd ..
)