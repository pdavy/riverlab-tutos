@rem |¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯|
@rem | configuration file for eros simulations	|
@rem | arguments: computer_name version		|
@rem |__________________________________________|


:config
@if "%1" equ "" (@set computer=%COMPUTERNAME%) else (@set computer=%1)
goto:%computer%

:DAVY-PPC3
@rem set eros=D:\Dev\Riverlab\__sources\__riverlab.master\eros\bin\eros
@set eros=..\..\..\__riverlab.master\Eros\bin\eros
goto:config
:BUFFON2020-SERV
@set eros=..\..\..\software\eros_x64\eros
goto:config

:config
if "%2" equ "" (set version=9.0) else (@set version=%2)
@set eros=%eros%%version%

if defined version (@echo eros config: %eros% / version: %version%) else (@echo eros config: %eros%)
goto:eof