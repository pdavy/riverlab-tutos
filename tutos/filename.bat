@rem |¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯|
@rem | configuration file for eros simulations	|
@rem | arguments:  file_name			|
@rem |__________________________________________|

@echo off
@set file=%~f1
@set filepath=%~dp1
@set filename=%~n1
@set fileext=%~x1
goto :eof